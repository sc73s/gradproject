package edu.missouristate.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import edu.missouristate.User;

@Controller
public class SpringMvcController {

	/**
	 * Server is http://localhost:8080
	 * Get Mapping (Path) http://localhost:8080/example
	 * Php http://localhost:8080/example.php
	 * 
	 */
	@GetMapping(value="/") 
	public String getIndexPage() {
		return "login.html";
	}
	
	@GetMapping(value="/login") 
	public String getlogin() {
		return "form.html";
	}
	
	@GetMapping(value="/signup") 
	public String getsignup() {
		System.out.println("User Successfully registered");
		return "login.html";
	}
	
	@PostMapping(value="/login")
		public String form() {
		
			return "form.html";
		}
	
	@PostMapping("/loggedin")
	public String loggedUser(User user) {
	     
	    userRepo.save(user);
	     
	    return "Signed Up Successfully";
	}
	
}
