<!DOCTYPE html>
<html>

<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

/* Full-width input fields */
input[type=text], input[type=password] {
    width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}

/* Add a background color when the inputs get focus */
input[type=text]:focus, input[type=password]:focus {
    background-color: #ddd;
    outline: none;
}

.modal
{

    background: url("http://elcaminodelray.com/wp-content/uploads/2013/11/Top-View-Table.jpg");    
    background-size: 100% 100%;
    background-repeat: no-repeat;
    background-size: auto;

}

/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

button:hover {
    opacity:1;
}

/* Extra styles for the cancel button */
.cancelbtn {
    padding: 14px 20px;
    background-color: #f44336;
}

/* Float cancel and signup buttons and add an equal width */
.cancelbtn, .signupbtn {
  float: left;
  width: 50%;
}

/* Add padding to container elements */
.container {
    padding: 16px;
}



/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto 5% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 60%; /* Could be more or less, depending on screen size */
}

/* Style the horizontal ruler */
hr {
    border: 1px solid #f1f1f1;
    margin-bottom: 25px;
}
 
/* The Close Button (x) */
.close {
    position: absolute;
    right: 35px;
    top: 15px;
    font-size: 40px;
    font-weight: bold;
    color: #f1f1f1;
}

.close:hover,
.close:focus {
    color: #f44336;
    cursor: pointer;
}

/* Clear floats */
.clearfix::after {
    content: "";
    clear: both;
    display: table;
}

/* Change styles for cancel button and signup button on extra small screens */
@media screen and (max-width: 300px) {
    .cancelbtn, .signupbtn {
       width: 100%;
    }
}
</style>
<body>

<div id="id02" class="modal">
  <span onclick="homepage.jsp" class="close" title="Close Modal">&times;</span>
  <form class="modal-content" action="./ServletDetails" method="post">
    <div class="container">

      <h1>Sign Up</h1>
      <p>Please fill in this form to create an account.</p>
      <hr>
       <label for="BearPassID"><b>BearPassID</b></label>
      <input type="text" placeholder="Enter BearPassID" name="BearPassID" required>
      
      

      <label for="Password"><b>Password</b></label>
      <input type="Password" placeholder="Enter Password" name="Password" required>
      
      
      
       <label for="FirstName"><b>FirstName</b></label>
      <input type="FirstName" placeholder="Enter FirstName" name="FirstName" required>
      
            
     <label for="mobile"><b>LastName</b></label>
      <input type="text" placeholder="Enter LastName" name="LastName" required>
      
      <label for="email"><b>Email</b></label>
      <input type="text" placeholder="Enter Email" name="email" required>
      
      
      <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

      <div class="clearfix">
 <p> <a href="index.html" class="w3-btn w3-black">Cancel</a></p>
        <button type="submit" class="signupbtn" style="background:black; align:center">Sign Up</button>
      </div>
    </div>
     <p class="change_link"> Already a member ?<a href="/login" class="login"> Go and log in </a></p>
  </form>
</div>
<script >


</script>




</body>
</html>
